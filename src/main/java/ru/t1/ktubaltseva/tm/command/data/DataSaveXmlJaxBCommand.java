package ru.t1.ktubaltseva.tm.command.data;

import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.Domain;
import ru.t1.ktubaltseva.tm.exception.data.SaveDataException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class DataSaveXmlJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-xml-jaxb";

    @NotNull
    private final String DESC = "Save data to xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws SaveDataException {
        System.out.println("[SAVE XML DATA]");
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_XML);
            @NotNull final Path path = file.toPath();

            Files.deleteIfExists(path);
            Files.createFile(path);

            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            jaxbMarshaller.marshal(domain, fileOutputStream);
            fileOutputStream.flush();
        } catch (IOException | JAXBException e) {
            throw new SaveDataException();
        }
    }

}
