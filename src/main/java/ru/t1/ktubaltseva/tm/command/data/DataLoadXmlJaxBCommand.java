package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.Domain;
import ru.t1.ktubaltseva.tm.exception.data.LoadDataException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataLoadXmlJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-load-xml-jaxb";

    @NotNull
    private final String DESC = "Load data from xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws LoadDataException {
        System.out.println("[LOAD XML DATA]");
        try {
            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            @NotNull final File file = new File(FILE_XML);
            @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
            setDomain(domain);
        } catch (JAXBException e) {
            throw new LoadDataException();
        }
    }

}
