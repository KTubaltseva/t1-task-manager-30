package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.EmailEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.LoginEmptyException;
import ru.t1.ktubaltseva.tm.model.User;

import java.security.NoSuchAlgorithmException;

public interface IUserService extends IService<User> {

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password
    ) throws NoSuchAlgorithmException, AbstractException;

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws AbstractException, NoSuchAlgorithmException;

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    ) throws AbstractException, NoSuchAlgorithmException;

    @NotNull
    User findByLogin(@Nullable String login) throws LoginEmptyException, UserNotFoundException;

    @NotNull
    User findByEmail(@Nullable String email) throws EmailEmptyException, UserNotFoundException;

    @NotNull
    Boolean isLoginExists(@Nullable String login) throws LoginEmptyException;

    @NotNull
    Boolean isEmailExists(@Nullable String email) throws EmailEmptyException;

    void lockUserByLogin(@Nullable String login) throws UserNotFoundException, LoginEmptyException;

    @NotNull
    User removeByLogin(@Nullable String login) throws UserNotFoundException, LoginEmptyException;

    @NotNull
    User removeByEmail(@Nullable String email) throws EmailEmptyException, UserNotFoundException;

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password
    ) throws NoSuchAlgorithmException, AbstractException;

    void unlockUserByLogin(@Nullable String login) throws UserNotFoundException, LoginEmptyException;

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName
    ) throws UserNotFoundException, IdEmptyException;

}
