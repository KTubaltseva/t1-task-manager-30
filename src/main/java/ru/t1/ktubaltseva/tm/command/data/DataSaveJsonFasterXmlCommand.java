package ru.t1.ktubaltseva.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.Domain;
import ru.t1.ktubaltseva.tm.exception.data.SaveDataException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class DataSaveJsonFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-json-faster-xml";

    @NotNull
    private final String DESC = "Save data to json file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws SaveDataException {
        System.out.println("[SAVE JSON DATA]");
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_JSON);
            @NotNull final Path path = file.toPath();

            Files.deleteIfExists(path);
            Files.createFile(path);
            
            @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(json.getBytes());
            fileOutputStream.flush();

        } catch (IOException e) {
            throw new SaveDataException();
        }
    }

}
