package ru.t1.ktubaltseva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "about";

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    private final String DESC = "Display developer info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService service = getPropertyService();
        System.out.println("[ABOUT]");
        System.out.println("Author: " + service.getAuthorName());
        System.out.println("E-mail: " + service.getAuthorEmail());
        System.out.println();
        System.out.println("[GIT]");
        System.out.println("Branch: " + service.getGitBranch());
        System.out.println("Commit Id: " + service.getGitCommitId());
        System.out.println("Committer: " + service.getGitCommitterName());
        System.out.println("E-mail: " + service.getGitCommitterEmail());
        System.out.println("Message: " + service.getGitCommitMessage());
        System.out.println("Time: " + service.getGitCommitTime());
    }

}
